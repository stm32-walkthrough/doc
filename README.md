# doc
Documentation for the STM32 guides done in latex. There is a documentation for using the [STM32CubeIDE](https://gitlab.com/stm32-walkthrough/doc/-/raw/main/walkthrough/stm32_walkthrough.pdf) and for using the [rm1s3_driver](https://gitlab.com/stm32-walkthrough/doc/-/raw/main/rm1s3_driver/rm1s3_driver.pdf).
