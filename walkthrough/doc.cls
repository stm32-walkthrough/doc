\DeclareOption*{%
  \PassOptionsToClass{\CurrentOption}{report}%
}
\ProcessOptions\relax
\LoadClass[addpoints,a4paper]{report}



\usepackage{redsgeneral}

\usepackage{redsmacros}

\usepackage{dirtree}


% Allows to define the document reference
\def\docref#1{\def\@docref{#1}}
% Allows to define the date
\def\thedate#1{\def\@thedate{#1}}

\docref{\jobname.tex}
\thedate{\today}
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Page layout
%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\extraheadheight{-1.5cm}
%\pagestyle{headandfoot}
%\footer{\@pvtitle}{\thepage}{\@pvdate}
%\footrule

\RequirePackage{fancyhdr}

\pagestyle{fancy}
\fancyhf{} %delete the current section for header and footer
\chead{
\hspace{-10pt}
\renewcommand{\arraystretch}{1.5}
\begin{tabular}{|m{60pt}|m{199pt}m{100pt}|m{60pt}|}
%\begin{tabularx}{\textwidth}{|c|c|c|c|}
\hline
% & & & \\
%logo-heig-vd & tit & tit & logo-reds \\ \hline 
%\multicolumn{2}{|l|}{Doc ref: } & Date: \date & Page \thepage \\ \hline
\raisebox{-.5\height}{\hspace{-.2cm}\includegraphics[width=70pt]{logo-heig-vd}} & \multicolumn{2}{m{299pt}|}{\begin{center}\large{\textbf{\MyTitle}}\end{center}} & \raisebox{-.5\height}{\includegraphics[width=60pt]{logo-reds}} \\ \hline 
\multicolumn{2}{|m{259pt}|}{Doc ref: \@docref} & \multicolumn{1}{c|}{Date: \@thedate} & \multicolumn{1}{c|}{Page \thepage/\pageref*{LastPage}} \\ \hline
\end{tabular}
}

%\begin{tabular}{|m{90pt}m{169pt}m{100pt}m{60pt}|}
%%\begin{tabularx}{\textwidth}{|c|c|c|c|}
%\hline
%% & & & \\
%%logo-heig-vd & tit & tit & logo-reds \\ \hline 
%%\multicolumn{2}{|l|}{Doc ref: } & Date: \date & Page \thepage \\ \hline
%\raisebox{-.5\height}{\includegraphics[height=1cm]{logo-heig-vd}} & \multicolumn{2}{m{269pt}}{\begin{center}\large{\MyTitle}\end{center}} & \raisebox{-.5\height}{\includegraphics[height=1cm]{logo-reds}} \\ \hline 
%\multicolumn{2}{|m{259pt}|}{Doc ref: \jobname.tex} & \multicolumn{1}{c|}{Date: \today} & \multicolumn{1}{c|}{Page \thepage/\pageref*{LastPage}} \\ \hline
%\end{tabular}


\renewcommand{\headrulewidth}{0pt}
%\fancyfoot[C]{Math2mat Scientific Report \copyright HES-SO // ISYS}
\fancyheadoffset[LE,RO]{.5cm} 
%\fancyfootoffset[LE,RO]{2.5cm}


\pagestyle{fancyplain} % options: empty , plain , fancy

%\usepackage{showframe}
%\usepackage{layout}

\usepackage[a4paper,top=2cm, bottom=1.25in, left=1in, right=1in]{geometry}
%\usepackage[a4paper,indluceheadfoot,margin=1cm]{geometry}

\setlength{\headheight}{55pt}
%\setlength{\oddsidemargin}{18pt}
%\setlength{\voffset}{-2.5cm}
%\setlength{\headsep}{1.5cm}





\setlength{\parindent}{0pt}








\renewcommand{\arraystretch}{1.5}


