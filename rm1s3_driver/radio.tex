
\chapter{rm1s3\_driver}

\section{Introduction}
The rm1s3\_driver is a driver used for communicating with a radio module with a RM1S3 firmware. The driver is targeted for the STM32 microcontroller family. The driver configure the radio into a star network where each node has a dedicated slot for avoiding collision. One node is configured as server and the other are clients, the server can communicate with every clients but clients can only communicate with the server. The server will consume more power than the clients, so it's advised to have the server externally powered and the clients battery powered. By modifying the driver (look at \verb|CMD_SET_DATA_SLOT_RANGE_TYPE|), we can allow inter client communication but it will lead to more power consumption.

\section{Installation}
prerequisite:
\begin{itemize}
    \item Have a STM32CubeIDE project
    \item With \verb|HAL_UART_MODULE_ENABLED| in \file{stm32l4xx_hal_conf.h}
    \item With HAL UART files (\file{stm32l4xx_hal_uart}, \file{stm32l4xx_hal_uart_ex})
    \item GPIO clock enable with \verb|__HAL_RCC_GPIOx_CLK_ENABLE()|
    \item the project use git
\end{itemize}

\medskip

Here is a step by step guide to add the driver to a STM32CubeIDE project.
\begin{itemize}
    \item Open a git bash on the root of your project
    \begin{itemize}
        \item \verb|git submodule add https://gitlab.com/stm32-walkthrough/rm1s3_driver Drivers/rm1s3_driver|
    \end{itemize}
    \item Copy \file{Drivers/rm1s3_driver/radio_config_template.h} to \file{Core/Inc/radio_config.h}
    \item Edit \file{radio_config.h} to match your hardware
    \begin{itemize}
        \item Unconnected pins can take the value \verb|UNCONNECTED_PIN|
        \item Set \verb|ISM_UART_NUMBER| with the U(S)ART number, use negative value for LPUART
        \item \verb|ISM_UART_SWAP_TX_RX_PINS| can be added to swap RX and TX pins
        \item Information about DMA configuration can be found in the chapter "DMA request mapping" of the STM32 Reference Manual.
    \end{itemize}
    \item In STM32CubeIDE, open the project properties
    \begin{itemize}
        \item C/C++ General -> Paths and Symbols -> Includes -> GNU C -> Add... -> \folder{Drivers/rm1s3_driver/Inc}
    \end{itemize}
\end{itemize}

\pagebreak

\section{Usage}
\begin{itemize}
    \item Edit the \file{main.c}
    \begin{itemize}
        \item Add \verb|#include "ism3.h"|
        \item Add in main() \verb|ism_init(NULL, NULL, NULL, NULL, NULL)|
        \item Add in the main loop \verb|ism_tick()|
    \end{itemize}
    \item Copy \file{Drivers/rm1s3_driver/radio_config_template.h} to \file{Core/Inc/radio_config.h}
    \item Edit \file{radio_config.h} to match your hardware
    \begin{itemize}
        \item Unconnected pins can take the value \verb|UNCONNECTED_PIN|
        \item \verb|ISM_UART_SWAP_TX_RX_PINS| can be added to swap RX and TX pins
        \item Information about DMA configuration can be found in the chapter "DMA request mapping" of the STM32 Reference Manual.
    \end{itemize}
    \item In STM32CubeIDE, open the project properties
    \begin{itemize}
        \item C/C++ General -> Paths and Symbols -> Includes -> GNU C -> Add... -> \folder{Drivers/rm1s3_driver/Inc}
    \end{itemize}
\end{itemize}

\subsection{Protocol configuration}
In the \file{radio_config.h}, we can also configure the following parameters of the radio protocol:
\begin{itemize}
    \item \verb|ISM_DATA_SLOT_COUNT|: the maximum number of node in the network
    \item \verb|ISM_RETRY_COUNT|: the maximum number of retry an unicast transmission will try
    \item \verb|ISM_SYNC_RX_INTERVAL|: the interval between synchronization reception
    \item \verb|ISM_SYNC_RX_MISS_MAX|: the maximum number of consecutive synchronization frame missed for considering a client is desynchronized
    \item \verb|ISM_SCAN_FIRST_ON_DURATION|: the duration in millisecond of the first active phase of scan
    \item \verb|ISM_SCAN_ON_DURATION|: the duration in millisecond of the active phases of scan
    \item \verb|ISM_SCAN_OFF_DURATION|: the duration in millisecond of the sleep phase of scan
    \item \verb|ISM_PATTERN|: the pattern to use for detecting the radio frame. Should be the same on all the devices. Can use a different pattern if we want to make two networks that should never communicate together.    
\end{itemize}
The scan is what allow a client to synchronize to the server, the parametrization allow compromise between power consumption and time needed to synchronize.

\subsection{Configuration}
The configuration is done just after calling is \verb|ism_init()|. The radio network need to have one module (server) that provide the synchronization to the other modules (clients). It's done by calling \newline\verb|ism_set_sync_mode(SM_TX)| for the server and calling \verb|ism_set_sync_mode(SM_RX_ACTIVE)| for the clients. We also need to call \verb|ism_config()| with the following parameters: 
\begin{itemize}
    \item address: should be unique in the network and lower than \verb|ISM_DATA_SLOT_COUNT|, it's recommended to use 0 for the server
    \item group: bit array that tell which multicast frame to receive
    \item power: set the tx power, should be less than or equal to \verb|ISM_MAX_POWER|
    \item power\_dbm: set the tx power in dBm, ignored if power != \verb|ISM_INVALID_POWER|, should be less than or equal to \verb|ISM_MAX_POWER_DBM|
    \item associated\_beacon\_id: advanced functionality, leave it at 0
\end{itemize}

\pagebreak

\subsection{Transmission}
The radio module allow two type of communication. The unicast allow to transmit data to a single destination, acknowledgement and retransmission provide good reliability. The unicast is done by calling \verb|ism_tx_unicast()|, only the module that have an address that match the destination will receive the frame. The multicast allow to transmit data to multiple destinations, acknowledge can't be used but multiple similar frame can be transmitted with a countdown value to allow some frame to fail. The multicast is done by calling \verb|ism_tx_multicast()|, all the modules that have \verb|group & dest_group != 0| will receive the frames.

\subsection{Reception}
For receiving radio data, we need to implement the unicast or multicast callback and register it when calling \verb|ism_init()|. All the callbacks are called from \verb|ism_tick()| so there is no concurrency problem with the main loop. The callback provide the received data but also the address of the source, the RSSI and the link quality.

\section{Power optimization}

The radio protocol is optimized for reducing the power consumption of the clients to the detriment of the server.

\subsection{Scan}

The scan is what allow a client to synchronize to the server, the parametrization allow compromise between power consumption and time needed to synchronize.

During the scan, the radio module consumption is ~10uA during off phase and ~35mA during on phase. Because synchronization frame are transmitted each 200ms by the server, on phase duration of 202ms allow enough time to receive a complete synchronization frame. To avoid that all the clients resynchronize at the same time after a down time of the server, a random value of 0 to 20 seconds is added to the off phase. A longer duration can be used for the first active phase to allow a faster synchronization just after a desynchronisation or after a power on. If the client can have long period of scan, we can calculate the average current consumption by applying the following formulas:

\[t\_scan\_on = \frac{ISM\_SCAN\_ON\_DURATION}{1000}\]
\[t\_scan\_off_{avg} = \frac{ISM\_SCAN\_OFF\_DURATION}{1000} + \frac{20s}{2}\]
\[Iscan_{avg} = \frac{t\_scan\_on \times 35mA + t\_scan\_off_{avg} \times 10uA}{t\_scan\_on + t\_scan\_off_{avg}}\]

\pagebreak

\subsection{Synchronization mode}

The client have 3 possible synchronization modes and can be set with \verb|ism_set_sync_mode()|.

\begin{tabular}{r|p{10cm}} 
    sync mode & description \\ \hline
    SM\_RX\_ACTIVE & Listen to all synchronization frames \\
    SM\_RX\_LOW\_POWER & Listen to synchronization frames each 5 seconds, data reception is desactivated but data transmission is unaffected \\
    SM\_RX\_LOW\_POWER\_GROUP & The module switch between the 2 precedent descriptions in function of the low power group set in the synchronization frame by the server \\
\end{tabular}

Currently the SM\_RX\_LOW\_POWER\_GROUP can be used only if the server doesn't use this driver because the change of the low power group is not implemented.

\subsection{UART baudrate}

The radio module use a STM32L4 with a LPUART to communicate with the host. The LPUART use the HSI (High Speed Internal) clock and when the STM32 is in low power, the LPUART need to have enough time to start the HSI to be able to correctly decode the incoming UART data. Because the deeper the low power mode, the longer it take to wakeup, we have the following dependency between baudrate and low power mode:

\begin{tabular}{r|l} 
    baudrate & low power mode \\ \hline
    $\leq$ 30000 & stop2 \\
    30001-14999 & stop0 \\
    > 150000 & sleep \\
\end{tabular}

The same problem occur for the firmware using the driver:

\begin{tabular}{r|c c} 
    & \multicolumn{2}{c}{low power mode} \\
    baudrate & LPUART & USART \\ \hline
    $\leq$ 30000 & stop2 & stop1 \\
    30001-14999 & stop1  & stop1 \\
    > 150000 & sleep & sleep \\
\end{tabular}

The deeper low power mode should only be used when the radio driver is ready otherwise only the sleep mode should be used. Here is a code example for a LPUART with low baudrate:

\begin{verbatim}
if (ism_is_ready()) {
    HAL_SuspendTick();
    HAL_PWREx_EnterSTOP2Mode(PWR_STOPENTRY_WFI);
    SystemClock_Config(); // clock config is reseted in stop mode
    HAL_ResumeTick();
} else {
    HAL_PWR_EnterSLEEPMode(PWR_MAINREGULATOR_ON, PWR_SLEEPENTRY_WFI);
}
\end{verbatim}

\pagebreak

\subsubsection{Temperature range}

The 30000 and 150000 baudrate limits are coded into the radio module but the real limit are dependant of the precision of the HSI that depend on temperature.

The STM32L4 user guide give a tolerance of the LPUART receiver to clock deviation of 4.42\% for 8N1 with baudrate lower than 1 Mbaud. With 1 start bit, 8 data bits and 1 stop bit, the bit\_count = 10.

\[DWU_{max} = LPUART\_rx\_tolerance - Osc\_max\_deviation\]
\[baudrate_{max} = \frac{bit\_count \times DWU_{max}}{t_{WULPUART}}\]

\begin{figure}[h]
    \includegraphics[width=\textwidth]{./images/HSI16 frequency versus temperature}
    \centering
    \caption{HSI16 frequency vs temperature from STM32L486xx datasheet}
\end{figure}

\begin{figure}[h]
    \includegraphics[width=\textwidth]{./images/Wakeup time LPUART}
    \centering
    \caption{Wakeup time using USART/LPUART from STM32L486xx datasheet}
\end{figure}
